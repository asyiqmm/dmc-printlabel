<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<!-- Brand Logo -->
	<!--<a href="<?php echo base_url() ?>assets/index3.html" class="brand-link">-->
	<!--	<img src="<?php echo base_url() ?>assets/dist/img/EngineerLogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">-->
	<!--	<span class="brand-text font-weight-light">DMC Engineer</span>-->
	<!--</a>-->

	<!-- Sidebar -->
	<div class="sidebar">
		<!-- Sidebar user (optional) -->
		<div class="user-panel mt-3 pb-3 mb-3 d-flex">
			<div class="image">
				<img src="<?php echo base_url() ?>assets/dist/img/logodmc.png" class="img-circle elevation-2" alt="User Image">
			</div>
			<div class="info">
				<a href="#" class="d-block"><?php echo $users->first_name . ' ' . $users->last_name ?></a>
			</div>
		</div>

		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<!-- Add icons to the links using the .nav-icon class
			   with font-awesome or any other icon font library -->
				<?php foreach ($menus as $menu) { ?>

					<li class="nav-item <?php echo $menu['id'] == $parent_active ? "menu-open " : "" ?>">
						<a class="nav-link <?php echo $menu['id'] == $parent_active ? "active " : "" ?>" href="<?php echo $menu['url'] != null ? base_url() . $menu['url'] : '#' ?>">

							<i class="nav-icon fas <?php echo $menu['icon'] ?>"></i>
							<p>
								<?php echo $menu['name'] ?>

								<?php if (array_key_exists('child', $menu)) { ?>
									<i class="right fas fa-angle-left"></i>
							</p>
						</a>

						<ul class="nav nav-treeview">
							<?php foreach ($menu['child'] as $child) {  ?>
								<li class="nav-item <?php echo $child['id'] == $child_active ? "menu-open" : "" ?>">
									<a href="<?php echo base_url() . $child['url'] ?>" class="nav-link <?php echo $child['id'] == $child_active ? "active" : "" ?>">
										<i class="<?php echo $child['id'] == $child_active ? "far" : "far" ?> <?php echo $child['icon'] ?> nav-icon"></i>
										<p><?php echo $child['name'] ?>
											<?php if (array_key_exists('grandchild', $child)) { ?>
												<i class="fas fa-angle-left right"></i>
										</p>
									</a>
									<ul class="nav nav-treeview">
										<?php foreach ($child['grandchild'] as $gchild) {  ?>
											<li class="nav-item">
												<a href="<?php echo base_url() . $gchild['url'] ?>" class="nav-link <?php echo $gchild['id'] == $grandchild_active ? "active" : "" ?>">
													<!-- <a href="pages/examples/login.html" class="nav-link"> -->
													<i class="<?php echo $gchild['id'] == $grandchild_active ? "far" : "far" ?> <?php echo $gchild['icon'] ?> nav-icon"></i>
													<!-- <i class="far fa-circle nav-icon"></i> -->
													<!-- <p>Login v1</p> -->
													<p><?php echo $gchild['name'] ?>
													</p>
														<!-- </a> -->
												</a>
											</li>
										<?php } ?>
									</ul>
								<?php } else { ?>
									</p>
									</a>
								<?php
											} ?>
								</li>
							<?php } ?>

						</ul>
					<?php } else { ?>
						</p>
						</a>
					<?php
								} ?>

					</li>
				<?php } ?>

			</ul>
		</nav>
		<!-- /.sidebar-menu -->
	</div>
	<!-- /.sidebar -->
</aside>