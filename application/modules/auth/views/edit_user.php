<!DOCTYPE html>
<html>

<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>DMC | Edit User</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- Font Awesome -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/fontawesome-free/css/all.min.css">
      <!-- Ionicons -->
      <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
      <!-- icheck bootstrap -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
      <!-- Theme style -->
      <link rel="stylesheet" href="<?php echo base_url() ?>assets/dist/css/adminlte.min.css">
      <!-- Google Font: Source Sans Pro -->
      <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body class="hold-transition register-page">
      <div class="register-box">
            <div class="register-logo">
                  <a href="<?php echo base_url() ?>dashboard/"><b>DMC</b> CMMS</a>
            </div>

            <div class="card">
                  <div class="card-body register-card-body">
                        <p class="login-box-msg">Edit member</p>

                        <?php echo form_open(uri_string()); ?>

                        <div class="form-group">
                              <label for="first_name">First Name:</label>
                              <?php echo form_input($first_name); ?>
                        </div>
                        <div class="form-group">
                              <label for="last_name">Last Name:</label>
                              <?php echo form_input($last_name); ?>

                        </div>
                        <div class="form-group">
                              <label for="username">Username:</label>
                              <!-- <input type="text" name="username" class="form-control" id="username" placeholder="Username"> -->
                              <?php echo form_input($identity); ?>

                        </div>
                        
                        <div class="form-group">
                              <?php echo lang('edit_user_password_label', 'password'); ?>
                              <!-- <input type="password" name="password" class="form-control" id="password" placeholder="Enter password"> -->
                              <?php echo form_input($password); ?>

                        </div>
                        <div class="form-group">
                              <?php echo lang('edit_user_password_confirm_label', 'password_confirm'); ?>
                              <!-- <input type="password" name="password_confirm" class="form-control" id="password_confirm" placeholder="Confirm password"> -->
                              <?php echo form_input($password_confirm); ?>
                        </div>
                        <?php if ($this->ion_auth->is_admin()) : ?>
                              <div class="form-group">
                              <!--<h3><?php echo lang('edit_user_groups_heading'); ?></h3>-->
                              <!--<label for="username">Level user:</label>-->
                              <?php foreach ($groups as $group) : ?>
                                    <label class="radio">
                                          <?php
                                          $gID = $group['id'];
                                          $checked = null;
                                          $item = null;
                                          foreach ($currentGroups as $grp) {
                                                if ($gID == $grp->id) {
                                                      $checked = ' checked="checked"';
                                                      break;
                                                }
                                          }
                                          ?>
                                          <input type="radio" class="" name="groups[]" value="<?php echo $group['id']; ?>" <?php echo $checked; ?>>
                                          <?php echo htmlspecialchars($group['description'], ENT_QUOTES, 'UTF-8'); ?>
                                    </label>
                              <?php endforeach ?>
                              </div>
                        <?php endif ?>
                        <?php echo form_hidden('id', $user->id); ?>
                        <?php echo form_hidden($csrf); ?>
                        <div class="row">
                              <div class="col-4">
                                    <button type="button" class="btn btn-secondary btn-block" onclick="goBack()">Back</button>

                              </div>
                              <div class="col-4">

                              </div>
                              <!-- /.col -->
                              <div class="col-4">
                                    <button type="submit" class="btn btn-primary btn-block">Save</button>
                              </div>
                              <!-- /.col -->
                        </div>
                        </form>
                  </div>
                  <!-- /.form-box -->
            </div><!-- /.card -->
      </div>
      <!-- /.register-box -->

      <!-- jQuery -->
      <script src="<?php echo base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
      <!-- Bootstrap 4 -->
      <script src="<?php echo base_url() ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- AdminLTE App -->
      <script src="<?php echo base_url() ?>assets/dist/js/adminlte.min.js"></script>
      <script src="<?php echo base_url() ?>assets/plugins/jquery-validation/jquery.validate.min.js"></script>
      <script src="<?php echo base_url() ?>assets/plugins/jquery-validation/additional-methods.min.js"></script>
      <script>
            function goBack() {
                  window.history.back();
            }
      </script>

</body>

</html>