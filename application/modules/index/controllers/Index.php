<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Index extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('print_model');
	}

	private function _render($view, $data = array())
	{
		$menu = $this->print_model->get_menu();
		$header = [];
		$list_menu_id = [];
		foreach ($menu as $item) {
			array_push($list_menu_id, $item->id);
			if ($item->parent_id == null) {
				$header[$item->menu_id]['id'] = $item->menu_id;
				$header[$item->menu_id]['name'] = $item->menu_nm;
				$header[$item->menu_id]['parent'] = $item->parent_id;
				$header[$item->menu_id]['icon'] = $item->icon;
				$header[$item->menu_id]['url'] = $item->url;
			} elseif ($item->child_id == null) {
				$header[$item->parent_id]['child'][$item->menu_id]['id'] = $item->menu_id;
				$header[$item->parent_id]['child'][$item->menu_id]['name'] = $item->menu_nm;
				$header[$item->parent_id]['child'][$item->menu_id]['parent'] = $item->parent_id;
				$header[$item->parent_id]['child'][$item->menu_id]['icon'] = $item->icon;
				$header[$item->parent_id]['child'][$item->menu_id]['url'] = $item->url;
			} else {
				$header[$item->parent_id]['child'][$item->child_id]['grandchild'][$item->menu_id]['id'] = $item->menu_id;
				$header[$item->parent_id]['child'][$item->child_id]['grandchild'][$item->menu_id]['name'] = $item->menu_nm;
				$header[$item->parent_id]['child'][$item->child_id]['grandchild'][$item->menu_id]['parent'] = $item->parent_id;
				$header[$item->parent_id]['child'][$item->child_id]['grandchild'][$item->menu_id]['grandchild'] = $item->child_id;
				$header[$item->parent_id]['child'][$item->child_id]['grandchild'][$item->menu_id]['icon'] = $item->icon;
				$header[$item->parent_id]['child'][$item->child_id]['grandchild'][$item->menu_id]['url'] = $item->url;
			}
		}
		$data['menus'] = $header;
		$data['title'] 	= "Print - PT.Dinamika Megatama Citra";
		$this->load->view('header', $data);
		$this->load->view('navbar');
		$this->load->view('sidebar', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		// var_dump('loij');die();
		$uri = &load_class('URI', 'core');
		// if ($this->ion_auth->logged_in()) {
		$data['parent_active'] = 1;
		$data['child_active'] = 1;
		$data['grandchild_active'] = 1;
		
		$data['material'] = $this->print_model->get_material()->result();
		// var_dump($data);die();
		$this->_render('print_view', $data);
	}

	public function print($id = null)
	{
		$uri = &load_class('URI', 'core');
		$get_data = $this->print_model->get_materialId($id);
		if ($get_data->num_rows() > 0) {
			$data['material'] = $get_data->row_array();
			$this->load->view('print_label',$data);
		}else {
			return show_error('Data tidak ditemukan.');
		}
	}

	public function list_riwayat()
	{
		$data = $this->print_model->list_riwayat();
		$obj = array("data" => $data);
		echo json_encode($obj);
	}

	public function post_form()
	{
		$post = $this->print_model->post_form();
		echo json_encode($post);
		// var_dump($_POST);die();
	}

	public function timbangan()
	{
		$this->load->library('curl');
		// $url = 'http://192.168.58.3/';
		$url = 'https://csrng.net/csrng/csrng.php?min=0&max=25';
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		$r = curl_exec($ch);
		curl_close($ch);
		$result = json_decode($r, true);
		$result = json_encode($result);
		echo $result;
	}

	public function delete(Type $var = null)
	{
		$delete = $this->print_model->delete();
		echo json_encode($delete);
	}
}
