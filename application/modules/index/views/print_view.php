<!-- Main Sidebar Container -->


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">

        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="container-fluid row">
            <div class="col-sm-4">
                <form action="" method="post" id="form" enctype="multipart/form-data" class="form-horizontal">
                    <div class="card ">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="">Pilih Material:</label>
                                        <select required name="id_material" id="id" width="100%" class="select2bs4">
                                            <option value="">Pilih Item Material</option>
                                            <?php foreach ($material as $key => $value) {
                                                echo '<option value="' . $value->id . '">' . $value->nama . '</option>';
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Qty Bag:</label>
                                        <input required type="number" class="form-control inputan col-sm-8 text-right" min=1 id="qty" autocomplete="off" name="bag">
                                    </div>
                                    <div class="form-group">
                                        <label for="">Berat:</label>
                                        <div class="input-group " data-target-input="nearest">
                                            <input required type="text" class="form-control decimal col-sm-9" width="50%" id="berat" autocomplete="off" name="weight">
                                            <div class="input-group-append">
                                                <div class="input-group-text">KG</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button type="submit" id="btnPrint" disabled class="btn btn-primary btn-sm"><i class="fas fa-print"></i> Simpan & Cetak</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-8">
                <div class="card ">
                    <div class="card-header">
                        <h3 class="card-title">Riwayat Transaksi</h3>
                    </div>
                    <div class="card-body">
                        <table id="tabelTransaksi" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th class="text-center text-nowrap">Tanggal</th>
                                    <th>Material</th>
                                    <th class="text-right">Qty Bag</th>
                                    <th class="text-right">Berat</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tfoot>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Material</th>
                                    <th>Qty Bag</th>
                                    <th>Berat</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Alasan hapus data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id="form_hapus" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body ">
                    <input type="hidden" name="id" id="id_mat">
                    <textarea name="keterangan" id="keterangan" cols="30" rows="5" class="form-control" placeholder="keterangan"></textarea>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="Submit" value="Submit" id="btnHapus" class="btn btn-danger">Hapus</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    function print() {
        window.location.href = "index/print/" + $('#id').val() + '/' + $('#qty').val() + '/' + $('#nilai_timbangan').text();
    }
    $(function() {
        var siteTable = $("#tabelTransaksi").DataTable({
            ajax: <?php base_url() ?> '/print_label/index/list_riwayat',
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "order": [
                [0, "desc"]
            ],
            buttons: [{
                    extend: 'pdf',
                    footer: true,
                    title: 'PDF',
                    className: 'btn btn-primary btn-sm',
                    exportOptions: {
                        columns: [0, 1, 2, 3]
                    }
                },
                {
                    extend: 'print',
                    footer: true,
                    title: 'Print',
                    className: 'btn btn-primary btn-sm',
                    exportOptions: {
                        columns: [0, 1, 2, 3]
                    }
                },
            ],
            initComplete: function() {
                this.api().buttons().container()
                    .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
            },
            columns: [{
                    data: 'created_date',
                    className: 'details-control align-middle text-center text-nowrap',
                    width: '1%'
                },
                {
                    data: 'nm_material',
                    className: 'details-control align-middle text-left',
                    width: '25%'
                },
                {
                    data: 'bag',
                    className: 'details-control align-middle text-center',
                    width: '10%'
                },
                {
                    data: 'weight',
                    className: 'details-control align-middle text-center',
                    width: '8%'
                },
                {
                    // data: '',
                    className: 'text-center align-middle',
                    defaultContent: '<button class="btn btn-sm btn-danger btn-hapus" title="hapus"><i class="fas fa-trash"></i></button>',
                    width: '1%'
                },
            ],
        });

        $('#tabelTransaksi tbody').on('click', '.btn-hapus', function() {
            var datas = siteTable.row($(this).parents('tr')).data();
            $('#modal-delete').modal('show');
            $('#id_mat').val(datas.id);
        });
    });

    // function timbang() {
    //     timeout = setTimeout(function() {
    //         get_timbang();
    //     }, 5000);
    // }

    $('#id,#qty,#berat').on('change', function() {
        if ($('#id').val() == "" || $('#qty').val() == "" || $('#qty').val() == 0 || $('#berat').val() == "" || Number($('#berat').val()) == 0) {
            $('#btnPrint').attr('disabled', true);
        } else {
            $('#btnPrint').attr('disabled', false);
        }
    })

    // function get_timbang() {
    //     console.log('jalan');
    //     $.ajax({
    //         url: <?php base_url() ?> 'index/timbangan',
    //         method: "GET",
    //         dataType: "json",
    //         success: function(data) {
    //             var adc = data[0];
    //             timbang();
    //             if (adc.status) {
    //                 // console.log(data.ADC);
    //                 var random = adc.random;
    //                 $('#alert_timbangan').hide();
    //                 // $('#btnSimpan').attr('disabled', false);
    //                 $('#nilai_timbangan').removeClass('text-danger');
    //                 $('#nilai_timbangan').text(random.toFixed(2));
    //                 $('#qty_timbang_makro').val(random.toFixed(2));
    //             } else {
    //                 $('#alert_timbangan').show();
    //                 $('#nilai_timbangan').addClass('text-danger');
    //                 // $('#btnSimpan').attr('disabled', true);
    //                 a = 0;
    //                 $('#nilai_timbangan').text(a.toFixed(2));
    //                 $('#qty_timbang_makro').val(a.toFixed(2));
    //             }
    //         },
    //         error: function(data) {
    //             timbang();
    //             $('#alert_timbangan').show();
    //             $('#nilai_timbangan').addClass('text-danger');
    //             $('#btnSimpan').attr('disabled', true);
    //             a = 0;
    //             $('#nilai_timbangan').text(a.toFixed(2));
    //             $('#qty_timbang_makro').val(a.toFixed(2));
    //             console.log(data);
    //         }
    //     })
    // }

    $('#form').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: <?php base_url() ?> '/print_label/index/post_form',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function() {
                $('#btnPrint').attr('disabled', true);
                swal.fire({
                    title: 'Please Wait..!',
                    text: 'Is working..',
                    onOpen: function() {
                        swal.showLoading()
                    }
                })
            },
            success: function(data) {
                if (data.status) {
                    var url = <?php base_url() ?> "index/print/" + data.id;
                    swal.fire({
                        icon: 'success',
                        title: 'Berhasil menambahkan data',
                        showConfirmButton: false,
                        timer: 2000
                    }).then(() => {
                        window.open(url, '_blank');
                    });
                } else {
                    swal.hideLoading();
                    swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                };
            },
            error: function() {
                swal.hideLoading();
                swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
            }
        });
    });

    $('#form_hapus').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: <?php base_url() ?> '/print_label/index/delete',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function() {
                $('#btnHapus').attr('disabled', true);
                swal.fire({
                    title: 'Please Wait..!',
                    text: 'Is working..',
                    onOpen: function() {
                        swal.showLoading()
                    }
                })
            },
            success: function(data) {
                if (data.status) {
                    $('#modal-delete').modal('hide');
                    swal.fire({
                        icon: 'success',
                        title: 'Berhasil menghapus data',
                        showConfirmButton: false,
                        timer: 2000
                    });
                    $('#btnHapus').attr('disabled', false);
                } else {
                    swal.hideLoading();
                    swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                    $('#btnHapus').attr('disabled', false);
                };
            },
            error: function() {
                swal.hideLoading();
                swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                $('#btnHapus').attr('disabled', false);
            }
        });
    });
</script>