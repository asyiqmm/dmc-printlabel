<div class="book">
    <div class="page">
        <div class="subpage">
            <table align="center" style="padding: 0px;" width="100%" media="print">
                <tbody align="center">
                    <tr style="font-size:0.13in;" class="pixolde">
                        <td colspan="2" style="border-bottom: 0.7px black solid;"><?php echo $material['nama']?></td>
                    </tr>
                    <tr style="font-size:0.1in;" class="pixolde">
                        <td width="50%" class="pixolde">BERAT</td>
                        <td width="50%">BAG</td>
                    </tr>
                    <tr style="font-weight: bold; font-size:0.19in" class="courier">
                        <td><?php echo $material['weight'] ?> KG</td>
                        <td><?php echo $material['bag'] ?> BAG</td>
                    </tr>
                    <tr style="font-size:8px" class="pixolde">
                        <td>D : <?php echo date('Y.m.d')?></td>
                        <td>T : <?php echo date('H:i')?></td>
                    </tr>
                    <tr style="font-size: 6px;" class="pixolde">
                        <td colspan="2">AYAM MURAH BY PT. DINAMIKA MEGATAMA CITRA</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<style>
    @font-face {
        font-family: pixolde;
        src: url('/print/assets/dist/font/pixolde.ttf');
    }

    @font-face {
        font-family: pixolde;
        src: url('/print/assets/dist/font/pixolde.ttf');
        font-weight: bold;
    }

    @font-face {
        font-family: TeXGyreCursor;
        src: url('/print/assets/dist/font/texgyrecursor-bold.ttf');
    }
    .pixolde {
        font-family: pixolde;
    }

    .courier {
        font-family: 'Courier New', Courier, monospace;
    }


    body {
        width: 2.1in;
        height: 0.78in;
        margin: 0.03in 0.08in 0in 0.08in;
        font-family: pixolde;
        padding: 0;
        background-color: #FAFAFA;
        font: 12pt "Tahoma";
    }

    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }

    .page {
        width: 2.1in;
        min-height: 0.78in;
        margin: 0.03in 0.08in 0in 0.04in;
        padding: 0;
        background: while;
    }

    .subpage {
        border: 2px black solid;
        border-radius: 10px;
        height: 0.78in;
    }

    @page {
        width: 2.1in;
        height: 0.78in;
        margin: 0.03in 0.08in 0in 0.04in;
    }

    @media print {

        html,
        body {
            width: 2.1in;
            height: 0.78in;
        }

        table,
        th,
        td {
            padding: 0px;
            margin: 0px;
        }

        .page {
            margin: 0.03in 0.04in 0in 0.02in;
            width: 2.1in;
            height: 0.78in;
            padding: 0in;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }


        .pixolde {
            font-family: pixolde;
        }

        .courier {
            font-family: 'Courier New', Courier, monospace;
        }
    }
</style>

<script>
    window.onload = function() {
        window.print();

    }
</script>