<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Print_model extends CI_Model
{
	public function get_menu()
	{
		$this->db->select('*')
			->from('m_menus as a')
			->where('a.active', 1)
			->order_by('a.orders', 'ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function get_material($id = null)
	{

		$this->db->where('active', 1);
		if ($id) {
			$this->db->where('id', $id);
		}
		// $result = $this->db->get('m_material');
		// var_dump($result->result());die();
		return $this->db->get('m_material');
	}

	public function get_materialId($id = null)
	{
		$this->db->select('nama,weight,bag')
			->from('m_material a')
			->join('t_transaksi b', 'b.id_material = a.id')
			->where('b.active', 1)
			->where('b.id', $id);
		return $query = $this->db->get();
	}

	public function list_riwayat()
	{
		$this->db->select("nama nm_material, bag, concat(weight,' KG') weight, b.created_date,b.id")
			->from('m_material a')
			->join('t_transaksi b', 'id_material = a.id')
			->where('b.active', 1);
		$query = $this->db->get();
		// $query = $this->db->last_query();
		// var_dump($query);die();
		return $query->result();
	}

	public function post_form()
	{
		$this->db->trans_begin();
		$this->db->insert('t_transaksi', $_POST);
		$id = $this->db->insert_id();
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'msg' => $this->db->error(),
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' 	=> true,
				'msg' 		=> 'Berhasil menambahkan data',
				'id'		=> $id
			];
		}
	}

	public function delete()
	{
		$this->db->trans_begin();
		$this->db->set('keterangan', $_POST['keterangan']);
		$this->db->set('active', 0)
		->where('id',$_POST['id'])
		->update('t_transaksi');
		// var_dump($this->db->last_query());die();
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'msg' => $this->db->error(),
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' 	=> true,
				'msg' 		=> 'Berhasil menghapus data',
			];
		}
	}
}
