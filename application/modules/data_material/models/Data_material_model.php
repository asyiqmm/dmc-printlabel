<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set('Asia/Jakarta');
class Data_material_model extends CI_Model
{
	public function get_menu()
	{
		$this->db->select('*')
			->from('m_menus as a')
			->where('a.active', 1)
			->order_by('a.orders', 'ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function list_material()
	{

		$this->db->where('active', 1);
		// $result = $this->db->get('m_material');
		// var_dump($result->result());die();
		return $this->db->get('m_material');
	}

	public function post_form()
	{
		$this->db->trans_begin();
		$this->db->insert('m_material', $_POST);
		// var_dump($this->db->last_query());die();
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'msg' => $this->db->error(),
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' 	=> true,
				'msg' 		=> 'Berhasil menambahkan material ' . $_POST['nama'],
			];
		}
	}

	public function delete()
	{
		$this->db->trans_begin();
		$this->db->set('active', 0)
			->where('id', $_POST['id'])
			->update('m_material');
		// var_dump($this->db->last_query());die();
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'msg' => $this->db->error(),
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' 	=> true,
				'msg' 		=> 'Berhasil menghapus data',
			];
		}
	}

	public function edit_material()
	{
		$this->db->trans_begin();
		$this->db->set('active', 0)
			->where('id', $_POST['id'])
			->update('m_material');
			$this->db->insert('m_material',$_POST['input']);
		
		if ($this->db->trans_status() === false) {
			$this->db->trans_rollback(); // transaksi rollback data jika ada salah satu query di atas yg error
			return [
				'status' => false,
				'msg' => $this->db->error(),
			];
		} else {
			$this->db->trans_commit();
			return [
				'status' 	=> true,
				'msg' 		=> 'Berhasil mengubah data',
			];
		}
	}
}
