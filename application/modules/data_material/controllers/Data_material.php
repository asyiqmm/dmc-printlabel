<?php
defined('BASEPATH') or exit('No direct script access allowed');
date_default_timezone_set('Asia/Jakarta');
class Data_material extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model('data_material_model');
	}

	private function _render($view, $data = array())
	{
		$menu = $this->data_material_model->get_menu();
		$header = [];
		$list_menu_id = [];
		foreach ($menu as $item) {
			array_push($list_menu_id, $item->id);
			if ($item->parent_id == null) {
				$header[$item->menu_id]['id'] = $item->menu_id;
				$header[$item->menu_id]['name'] = $item->menu_nm;
				$header[$item->menu_id]['parent'] = $item->parent_id;
				$header[$item->menu_id]['icon'] = $item->icon;
				$header[$item->menu_id]['url'] = $item->url;
			} elseif ($item->child_id == null) {
				$header[$item->parent_id]['child'][$item->menu_id]['id'] = $item->menu_id;
				$header[$item->parent_id]['child'][$item->menu_id]['name'] = $item->menu_nm;
				$header[$item->parent_id]['child'][$item->menu_id]['parent'] = $item->parent_id;
				$header[$item->parent_id]['child'][$item->menu_id]['icon'] = $item->icon;
				$header[$item->parent_id]['child'][$item->menu_id]['url'] = $item->url;
			} else {
				$header[$item->parent_id]['child'][$item->child_id]['grandchild'][$item->menu_id]['id'] = $item->menu_id;
				$header[$item->parent_id]['child'][$item->child_id]['grandchild'][$item->menu_id]['name'] = $item->menu_nm;
				$header[$item->parent_id]['child'][$item->child_id]['grandchild'][$item->menu_id]['parent'] = $item->parent_id;
				$header[$item->parent_id]['child'][$item->child_id]['grandchild'][$item->menu_id]['grandchild'] = $item->child_id;
				$header[$item->parent_id]['child'][$item->child_id]['grandchild'][$item->menu_id]['icon'] = $item->icon;
				$header[$item->parent_id]['child'][$item->child_id]['grandchild'][$item->menu_id]['url'] = $item->url;
			}
		}
		$data['menus'] = $header;
		$data['title'] 	= "Data Master Material - PT.Dinamika Megatama Citra";
		$this->load->view('header', $data);
		$this->load->view('navbar');
		$this->load->view('sidebar', $data);
		$this->load->view($view, $data);
		$this->load->view('footer');
	}

	public function index()
	{
		// var_dump('loij');die();
		$uri = &load_class('URI', 'core');
		// if ($this->ion_auth->logged_in()) {
		$data['parent_active'] = 2;
		$data['child_active'] = 3;
		$data['grandchild_active'] = null;
		
		$this->_render('material_view', $data);
	}

	public function list_material()
	{
		$data = $this->data_material_model->list_material()->result();
		$obj = array("data" => $data);
		echo json_encode($obj);
	}

	public function post_form()
	{
		// var_dump($_POST);die();
		$post = $this->data_material_model->post_form();
		echo json_encode($post);
	}

	public function delete(Type $var = null)
	{
		$delete = $this->data_material_model->delete();
		echo json_encode($delete);
	}

	public function edit_material(Type $var = null)
	{
		$edit = $this->data_material_model->edit_material();
		echo json_encode($edit);
	}
}
