<!-- Main Sidebar Container -->


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">

        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="container-fluid">

            <form action="" method="post" id="form" enctype="multipart/form-data" class="form-horizontal">
                <div class="card ">
                    <div class="card-body">
                        <table id="tabelMaterial" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama Material.</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </form>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>

<div class="modal fade" id="modal-add">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Tambah material baru</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id="form_material" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body ">
                    <div class="form-group">
                        <label for="nm_material">Nama Material:</label>
                        <input required type="text" class="form-control" name="nama" id="nm_material" placeholder="nama material">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="Submit" value="Submit" id="btnSubmit" class="btn btn-primary">Tambah</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal-edit">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit material</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" id="form_edit_material" enctype="multipart/form-data" class="form-horizontal">
                <div class="modal-body ">
                    <div class="form-group">
                        <label for="nm_material">Nama Material:</label>
                        <input type="hidden" name="id" id="id_material">
                        <input required type="text" class="form-control" name="input[nama]" id="edit_material" placeholder="nama material">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button type="Submit" value="Submit" id="btnEdit" class="btn btn-warning">Edit</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    function print() {
        window.location.href = "index/print/" + $('#id').val() + '/' + $('#qty').val() + '/' + $('#nilai_timbangan').text();
    }
    $(function() {
        var siteTable = $("#tabelMaterial").DataTable({
            ajax: <?php base_url() ?> '/print_label/data_material/list_material',
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "order": [
                [1, "desc"]
            ],
            buttons: [{
                    text: 'Tambah Material',
                    className: 'btn btn-primary btn-sm btn-add',
                    action: function(e, dt, node, config) {
                        $('.btn-add')
                            .attr('data-toggle', 'modal')
                            .attr('data-target', '#modal-add');
                    },
                }, {
                    extend: 'pdf',
                    footer: true,
                    title: 'PDF',
                    className: 'btn btn-primary btn-sm',
                    exportOptions: {
                        columns: [0, 1]
                    }
                },
                {
                    extend: 'print',
                    footer: true,
                    title: 'Print',
                    className: 'btn btn-primary btn-sm',
                    exportOptions: {
                        columns: [0, 1]
                    }
                },
            ],
            initComplete: function() {
                this.api().buttons().container()
                    .appendTo($('.col-md-6:eq(0)', this.api().table().container()));
            },
            columns: [{
                    data: null,
                    className: 'details-control align-middle text-center text-nowrap',
                    width: '1%'
                },
                {
                    data: 'nama',
                    className: 'details-control align-middle text-left',
                    width: '25%'
                },
                {
                    // data: '',
                    className: 'text-center align-middle text-nowrap',
                    defaultContent: '<button type="button" class="btn btn-sm btn-danger btn-hapus m-1" title="hapus"><i class="fas fa-trash"></i></button><button type="button" class="btn btn-sm btn-warning btn-edit" title="edit"><i class="fas fa-edit"></i></button>',
                    width: '1%'
                },
            ],
        });
        siteTable.on('order.dt search.dt', function() {
            siteTable.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        $('#tabelMaterial tbody').on('click', '.btn-hapus', function() {
            var datas = siteTable.row($(this).parents('tr')).data();
            console.log(datas);
            swal
                .fire({
                    title: "Perhatian",
                    html: "Hapus material  <b>" + datas.nama + "</b>?",
                    // type: "question",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: "#e74c3c",
                    confirmButtonText: "Hapus",
                    cancelButtonText: "Tidak"
                })
                .then(function(result) {
                    if (result.value) {
                        $.ajax({
                            url: <?php base_url() ?> 'data_material/delete',
                            method: "POST",
                            data: {
                                id: datas.id
                            },
                            beforeSend: function() {
                                swal.fire({
                                    title: 'Please Wait..!',
                                    text: 'Is working..',
                                    onOpen: function() {
                                        swal.showLoading()
                                    }
                                })
                            },
                            dataType: "json",
                            success: function(data) {
                                if (data.status == true) {
                                    $("#tabelMaterial")
                                        .DataTable()
                                        .ajax.reload();
                                    swal.fire({
                                        icon: 'success',
                                        title: data.msg,
                                        showConfirmButton: false,
                                        timer: 2000
                                    });
                                } else {
                                    swal.hideLoading();
                                    swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                                }
                            },
                            error: function() {
                                swal.hideLoading();
                                swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                            }
                        })
                    } else {}
                });
        });

        $('#tabelMaterial tbody').on('click', '.btn-edit', function() {
            $('#modal-edit').modal('show');
            var datas = siteTable.row($(this).parents('tr')).data();
            $('#id_material').val(datas.id);
            $('#edit_material').val(datas.nama);
        });

    });

    // function timbang() {
    //     timeout = setTimeout(function() {
    //         get_timbang();
    //     }, 5000);
    // }

    $('#id,#qty,#berat').on('change', function() {
        if ($('#id').val() == "" || $('#qty').val() == "" || $('#qty').val() == 0 || $('#berat').val() == "" || Number($('#berat').val()) == 0) {
            $('#btnPrint').attr('disabled', true);
        } else {
            $('#btnPrint').attr('disabled', false);
        }
    })

    $('#form_material').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: <?php base_url() ?> '/print_label/data_material/post_form',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function() {
                $('#btnSubmit').attr('disabled', true);
                swal.fire({
                    title: 'Please Wait..!',
                    text: 'Is working..',
                    onOpen: function() {
                        swal.showLoading()
                    }
                })
            },
            success: function(data) {
                if (data.status) {
                    $('#modal-add').modal('hide');
                    $('#nm_material').val('');
                    swal.fire({
                        icon: 'success',
                        title: data.msg,
                        showConfirmButton: false,
                        timer: 2000
                    }).then(() => {
                        $('#btnSubmit').attr('disabled', false);
                        $("#tabelMaterial")
                            .DataTable()
                            .ajax.reload();
                    });


                } else {
                    swal.hideLoading();
                    swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                    $('#btnSubmit').attr('disabled', false);
                };
            },
            error: function() {
                swal.hideLoading();
                swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                $('#btnSubmit').attr('disabled', false);
            }
        });
    });

    $('#form_edit_material').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: <?php base_url() ?> '/print_label/data_material/edit_material',
            type: "post",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            async: false,
            dataType: "JSON",
            beforeSend: function() {
                $('#btnEdit').attr('disabled', true);
                swal.fire({
                    title: 'Please Wait..!',
                    text: 'Is working..',
                    onOpen: function() {
                        swal.showLoading()
                    }
                })
            },
            success: function(data) {
                if (data.status) {
                    $('#modal-edit').modal('hide');
                    $('#nm_material').val('');
                    swal.fire({
                        icon: 'success',
                        title: data.msg,
                        showConfirmButton: false,
                        timer: 2000
                    }).then(() => {
                        $('#btnEdit').attr('disabled', false);
                        $("#tabelMaterial")
                            .DataTable()
                            .ajax.reload();
                    });


                } else {
                    swal.hideLoading();
                    swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
                    $('#btnEdit').attr('disabled', false);
                };
            },
            error: function() {
                swal.hideLoading();
                swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");

                $('#btnEdit').attr('disabled', false);
            }
        });
    });



    // $('#form_hapus').on('submit', function(event) {
    //     event.preventDefault();
    //     $.ajax({
    //         url: <?php base_url() ?> '/print_label/index/delete',
    //         type: "post",
    //         data: new FormData(this),
    //         processData: false,
    //         contentType: false,
    //         cache: false,
    //         async: false,
    //         dataType: "JSON",
    //         beforeSend: function() {
    //             $('#btnHapus').attr('disabled', true);
    //             swal.fire({
    //                 title: 'Please Wait..!',
    //                 text: 'Is working..',
    //                 onOpen: function() {
    //                     swal.showLoading()
    //                 }
    //             })
    //         },
    //         success: function(data) {
    //             if (data.status) {
    //                 $('#modal-delete').modal('hide');
    //                 swal.fire({
    //                     icon: 'success',
    //                     title: 'Berhasil menghapus data',
    //                     showConfirmButton: false,
    //                     timer: 2000
    //                 });
    //                 $('#btnHapus').attr('disabled', false);
    //             } else {
    //                 swal.hideLoading();
    //                 swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
    //                 $('#btnHapus').attr('disabled', false);
    //             };
    //         },
    //         error: function() {
    //             swal.hideLoading();
    //             swal.fire("!Opps ", "Terjadi masalah, coba lagi nanti", "error");
    //             $('#btnHapus').attr('disabled', false);
    //         }
    //     });
    // });
</script>